﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Test_class_OPC_Kepware
{
    public static class ConnectDatabase
    {
        public static string ConString = SystemData.ReadTxtfile("ConnectionString.Con");

        public static DataTable CallStore(string StoreProducer)
        {
            try
            {
                string Query = $"exec {StoreProducer};";
                SqlConnection conn = new SqlConnection(ConString);
                conn.Open();
                SqlCommand cmd = new SqlCommand(Query, conn);
                SqlDataAdapter returnVal = new SqlDataAdapter(Query, conn);
                DataTable dt = new DataTable();
                returnVal.Fill(dt);
                conn.Close();
                return dt;
            }
            catch
            {
                return null;
            }
        }
    }
}
