﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Test_class_OPC_Kepware
{
    public class Kepware_Tag
    {
        public int KepwareTagIndex { get; set; }
        public string KepwareTag { get; set; }
        public string Value { get; set; }
        public string Status { get; set; }
        public string Time { get; set; }
    }
}
