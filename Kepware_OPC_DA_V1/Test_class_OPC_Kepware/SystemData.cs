﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;

namespace Test_class_OPC_Kepware
{
    public static class SystemData
    {
        public static List<string> getfileName(string folder, string filter = "")
        {
            var foo = Directory.GetFiles(folder, filter);
            return foo.Select(o => Path.GetFileName(o)).ToList<string>();
        }

        #region save  template

        public static bool SaveTxt2file(string filepath, string txt)
        {
            try
            {
                filepath = AppDomain.CurrentDomain.BaseDirectory + filepath;
                File.WriteAllText(filepath, txt);
                return true;
            }
            catch
            {

                return false;
            }
        }

        public static string ReadTxtfile(string filepath)
        {
            try
            {
                filepath = AppDomain.CurrentDomain.BaseDirectory + filepath;
                return File.ReadAllText(filepath);
            }
            catch
            {
                return "";
            }
        }

        public static T JsontoObj<T>(string json)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            T obj = js.Deserialize<T>(json);

            return obj;
        }

        public static string ObjToJson<T>(T obj)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            var output = js.Serialize(obj);
            return output;
        }
        #endregion
    }
}
