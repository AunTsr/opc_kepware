﻿
using OPCAutomation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Test_class_OPC_Kepware
{
    public class kepService
    {
        public Dictionary<string, string> _tags { get; set; }
        public Dictionary<string, object> tags { get; set; }
        public Kepware_Tag _Tag;
        public string ServerIP { get; set; } = "127.0.0.1";
        public string ServerName { get; set; } = "Kepware.KEPServerEX.V6";
        public string GroupMame { get; set; } = "Group1";

        public OPCServer MyOPCServer;
        public OPCGroup MyOPCGroup;
        public OPCItem MyOPCItem;
        public OPCBrowser MyOPCBrowser;

        public Array MyServerHandles = Array.CreateInstance(typeof(int), 20);
        public Array MyValues = Array.CreateInstance(typeof(object), 20);
        public Array MyErrors = Array.CreateInstance(typeof(int), 20);
        public Array MyServerHandles1 = Array.CreateInstance(typeof(int), 2);
        public Array MyValues1 = Array.CreateInstance(typeof(int), 2);
        public Array MyErrors1 = Array.CreateInstance(typeof(int), 2);
        object quality = new object();
        object timestamp = new object();

        public bool IsComplate = false;

        public bool Connect()
        {
            tags = new Dictionary<string, object>();
            try
            {
                MyOPCServer = new OPCServer();
                MyOPCServer.Connect(ServerName, ServerIP);
                if (MyOPCServer.ServerState == 1)
                {
                    if (MyOPCGroup == null) MyOPCGroup = MyOPCServer.OPCGroups.Add("MyGroup_" + GroupMame);
                    MyOPCGroup.IsActive = true;
                    MyOPCGroup.IsSubscribed = true;
                    MyOPCGroup.UpdateRate = 1000;
                    MyOPCGroup.AsyncReadComplete += new DIOPCGroupEvent_AsyncReadCompleteEventHandler(ObjOPCGroup_AsyncReadComplete);
                    return true;
                }
                else return false;
            }
            catch (Exception ex)
            {
                //MessageABC aBC = new MessageABC();
                //aBC.message = ex.ToString();
                //aBC.Popup();
                return false;
            }
        }

        public bool Disconnect()
        {
            try
            {
                MyOPCGroup.IsActive = false;
                MyOPCGroup.IsSubscribed = false;
                MyOPCServer.OPCGroups.RemoveAll();
                MyOPCServer.Disconnect();
                return true;
            }
            catch (Exception ex)
            {
                //MessageABC aBC = new MessageABC();
                //aBC.message = ex.ToString();
                //aBC.Popup();
                return false;
            }
        }

        public bool InitTagsList(Dictionary<string, string> tags)
        {
            int count = 1;
            if (_tags != null) _tags.Clear();
            try
            {
                _tags = tags;
                for (int i = 0; i < tags.Count; i++)
                {
                    MyOPCItem = MyOPCGroup.OPCItems.AddItem(tags.Values.ElementAt(i), int.Parse(tags.Keys.ElementAt(i)));
                    MyServerHandles.SetValue(MyOPCItem.ServerHandle, count);
                    count++;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public object ReadAlltags()
        {
            var Date = DateTime.Now.Second;
            IsComplate = false;
            try
            {
                while (!IsComplate)
                {
                    MyOPCGroup.AsyncRead(_tags.Count, ref MyServerHandles, out MyErrors, Date, out Date);
                }
                return tags;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public object Readtagvalue(string tagname)
        {
            var Date = DateTime.Now.Second;
            try
            {
                int key = int.Parse(DictionaryGetValue.KeyByValue(_tags, tagname)) + 1;
                MyServerHandles1.SetValue(MyServerHandles.GetValue(key), 1);
                MyOPCGroup.SyncRead((short)OPCAutomation.OPCDataSource.OPCDevice, 1, ref MyServerHandles1, out MyValues1, out MyErrors1, out quality, out timestamp);

                _Tag = new Kepware_Tag();
                _Tag.KepwareTagIndex = key - 1;
                _Tag.KepwareTag = tagname;
                _Tag.Value = MyValues1.GetValue(1).ToString();

                string[] arrStatus = ((IEnumerable)quality).Cast<object>()
                                 .Select(x => x.ToString())
                                 .ToArray();

                string[] arrTime = ((IEnumerable)timestamp).Cast<object>()
                                .Select(x => x.ToString())
                                .ToArray();

                _Tag.Status = (arrStatus[0] == "192") ? "Good" : "Bad";
                _Tag.Time = arrTime[0];
                return _Tag;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void Updatetagsvalue()
        {
            MyOPCGroup.DataChange += new DIOPCGroupEvent_DataChangeEventHandler(ObjOPCGroup_DataChange);
        }

        public void Writetagvalue(string tagname, object val)
        {
            int key = int.Parse(DictionaryGetValue.KeyByValue(_tags, tagname)) + 1;
            MyServerHandles1.SetValue(MyServerHandles.GetValue(key), 1);
            MyValues1.SetValue(val.ToString(), 1);
            MyOPCGroup.SyncWrite(1, ref MyServerHandles1, ref MyValues1, out MyErrors1);
        }

        #region New_Function
        private void ObjOPCGroup_DataChange(int TransectionId, int NumItems, ref Array ClientHandles, ref Array ItemValues, ref Array Qualities, ref Array TimeStamps)
        {
            for (int i = 0; i < ItemValues.Length; i++)
            {
                int z = int.Parse(ClientHandles.GetValue(1 + 1).ToString());
                _Tag = new Kepware_Tag();
                _Tag.KepwareTagIndex = z;
                _Tag.KepwareTag = _tags[ClientHandles.GetValue(i + 1).ToString()].ToString();
                _Tag.Value = ItemValues.GetValue(i + 1).ToString();
                _Tag.Status = (Qualities.GetValue(i + 1).ToString() == "192") ? "Good" : "Bad";
                _Tag.Time = TimeStamps.GetValue(i + 1).ToString();
                tags[z.ToString()] = _Tag;
            }
        }

        private void ObjOPCGroup_AsyncReadComplete(int TransectionId, int NumItems, ref Array ClientHandles, ref Array ItemValues, ref Array Qualities, ref Array TimeStamps, ref Array Errors)
        {
            while (!IsComplate)
            {
                for (int i = 0; i < ItemValues.Length; i++)
                {
                    _Tag = new Kepware_Tag();
                    _Tag.KepwareTagIndex = int.Parse(ClientHandles.GetValue(i + 1).ToString());
                    _Tag.KepwareTag = _tags[ClientHandles.GetValue(i + 1).ToString()].ToString();
                    _Tag.Value = ItemValues.GetValue(i + 1).ToString();
                    _Tag.Status = (Qualities.GetValue(i + 1).ToString() == "192") ? "Good" : "Bad";
                    _Tag.Time = TimeStamps.GetValue(i + 1).ToString();
                    tags.Add(ClientHandles.GetValue(i + 1).ToString(), _Tag);
                }
                this.IsComplate = true;
            }
        }
        #endregion
    }
}
