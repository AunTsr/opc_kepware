﻿using OPCAutomation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Test_class_OPC_Kepware
{
    public partial class Form1 : Form
    {
        public DataTable DataRead;
        public Dictionary<string, string> _tags = new Dictionary<string, string>();
        public Dictionary<string, object> _Tags = new Dictionary<string, object>();
        Kepware_Tag tag = new Kepware_Tag();

        public OPCServer MyOPCServer;
        public OPCGroup MyOPCGroup;
        public OPCItem MyOPCItem;

        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            _tags.Clear();
            DataRead = ConnectDatabase.CallStore("aaa");
            foreach (DataRow dtRow in DataRead.Rows)
            {
                _tags.Add(dtRow["ID"].ToString().Trim(), dtRow["MachineTag"].ToString().Trim());
            }
            kepService kepTags = new kepService();
            kepTags.Connect();

            kepTags.InitTagsList(_tags);

            _Tags = (Dictionary<string, object>)kepTags.ReadAlltags();
            tag = (Kepware_Tag)kepTags.Readtagvalue(textBox1.Text);
            textBox2.Text = tag.Value;
        }
    }
}
