﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Test_class_OPC_Kepware
{
    public static class Tokey
    {
        public static T KeyByValue<T, W>(this Dictionary<T, W> dict, W val)
        {
            T key = default;
            foreach (KeyValuePair<T, W> pair in dict)
            {
                if (EqualityComparer<W>.Default.Equals(pair.Value, val))
                {
                    key = pair.Key;
                    break;
                }
            }
            return key;
        }
    }
}
