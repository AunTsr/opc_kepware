﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Test_class_OPC_Kepware
{
    public partial class MessageABC : Form
    {
        public string message { get; set; } = "aaa";
        public MessageABC()
        {
            InitializeComponent();
        }

        public void Popup()
        {
            label1.Text = message;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
