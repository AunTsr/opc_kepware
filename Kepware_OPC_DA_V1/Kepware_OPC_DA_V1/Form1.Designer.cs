﻿namespace Kepware_OPC_DA_V1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.dtgKepwareTag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtgValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtgStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtgTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.gbSetConnect = new System.Windows.Forms.GroupBox();
            this.txtIp = new System.Windows.Forms.TextBox();
            this.btConnect = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtServerName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btRunService = new System.Windows.Forms.Button();
            this.btStopService = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.btSave = new System.Windows.Forms.Button();
            this.btDelete = new System.Windows.Forms.Button();
            this.btSelect = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.colsTags = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btDisConnectCon = new System.Windows.Forms.Button();
            this.txtIpCon = new System.Windows.Forms.TextBox();
            this.BtConnectCon = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtServerNameCon = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.gbSetConnect.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.AliceBlue;
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(937, 666);
            this.panel1.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(937, 666);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel2);
            this.tabPage3.Controls.Add(this.flowLayoutPanel1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(929, 640);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "Service";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LightSkyBlue;
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.dataGridView3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 79);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(923, 558);
            this.panel2.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Monitoring Items:";
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dtgKepwareTag,
            this.dtgValue,
            this.dtgStatus,
            this.dtgTime});
            this.dataGridView3.Location = new System.Drawing.Point(0, 29);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(922, 532);
            this.dataGridView3.TabIndex = 0;
            // 
            // dtgKepwareTag
            // 
            this.dtgKepwareTag.HeaderText = "KepwareTag";
            this.dtgKepwareTag.Name = "dtgKepwareTag";
            this.dtgKepwareTag.Width = 200;
            // 
            // dtgValue
            // 
            this.dtgValue.HeaderText = "Value";
            this.dtgValue.Name = "dtgValue";
            // 
            // dtgStatus
            // 
            this.dtgStatus.HeaderText = "Status";
            this.dtgStatus.Name = "dtgStatus";
            // 
            // dtgTime
            // 
            this.dtgTime.HeaderText = "Time";
            this.dtgTime.Name = "dtgTime";
            this.dtgTime.Width = 200;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.LightSkyBlue;
            this.flowLayoutPanel1.Controls.Add(this.gbSetConnect);
            this.flowLayoutPanel1.Controls.Add(this.btRunService);
            this.flowLayoutPanel1.Controls.Add(this.btStopService);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(923, 76);
            this.flowLayoutPanel1.TabIndex = 9;
            // 
            // gbSetConnect
            // 
            this.gbSetConnect.Controls.Add(this.txtIp);
            this.gbSetConnect.Controls.Add(this.btConnect);
            this.gbSetConnect.Controls.Add(this.label3);
            this.gbSetConnect.Controls.Add(this.TxtServerName);
            this.gbSetConnect.Controls.Add(this.label4);
            this.gbSetConnect.Location = new System.Drawing.Point(3, 3);
            this.gbSetConnect.Name = "gbSetConnect";
            this.gbSetConnect.Size = new System.Drawing.Size(325, 71);
            this.gbSetConnect.TabIndex = 6;
            this.gbSetConnect.TabStop = false;
            this.gbSetConnect.Text = "SetConnect";
            // 
            // txtIp
            // 
            this.txtIp.Location = new System.Drawing.Point(66, 14);
            this.txtIp.Name = "txtIp";
            this.txtIp.Size = new System.Drawing.Size(146, 20);
            this.txtIp.TabIndex = 2;
            this.txtIp.Text = "127.0.0.1";
            // 
            // btConnect
            // 
            this.btConnect.Location = new System.Drawing.Point(227, 14);
            this.btConnect.Name = "btConnect";
            this.btConnect.Size = new System.Drawing.Size(76, 41);
            this.btConnect.TabIndex = 4;
            this.btConnect.Text = "Connect";
            this.btConnect.UseVisualStyleBackColor = true;
            this.btConnect.Click += new System.EventHandler(this.BtConnect_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "IP :";
            // 
            // TxtServerName
            // 
            this.TxtServerName.Location = new System.Drawing.Point(66, 35);
            this.TxtServerName.Name = "TxtServerName";
            this.TxtServerName.Size = new System.Drawing.Size(146, 20);
            this.TxtServerName.TabIndex = 3;
            this.TxtServerName.Text = "Kepware.KEPServerEX.V6";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "S Name :";
            // 
            // btRunService
            // 
            this.btRunService.Location = new System.Drawing.Point(334, 3);
            this.btRunService.Name = "btRunService";
            this.btRunService.Size = new System.Drawing.Size(100, 71);
            this.btRunService.TabIndex = 8;
            this.btRunService.Text = "RunService";
            this.btRunService.UseVisualStyleBackColor = true;
            this.btRunService.Click += new System.EventHandler(this.BtRunService_Click);
            // 
            // btStopService
            // 
            this.btStopService.Location = new System.Drawing.Point(440, 3);
            this.btStopService.Name = "btStopService";
            this.btStopService.Size = new System.Drawing.Size(100, 71);
            this.btStopService.TabIndex = 7;
            this.btStopService.Text = "StopService";
            this.btStopService.UseVisualStyleBackColor = true;
            this.btStopService.Click += new System.EventHandler(this.BtStopService_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.btSave);
            this.tabPage1.Controls.Add(this.btDelete);
            this.tabPage1.Controls.Add(this.btSelect);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(929, 640);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Config";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.listBox1);
            this.groupBox4.Location = new System.Drawing.Point(275, 94);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(239, 470);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Tags List";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(1, 24);
            this.listBox1.Margin = new System.Windows.Forms.Padding(0);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(238, 446);
            this.listBox1.TabIndex = 0;
            // 
            // btSave
            // 
            this.btSave.Location = new System.Drawing.Point(728, 570);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(172, 52);
            this.btSave.TabIndex = 9;
            this.btSave.Text = "Save";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.BtSave_Click);
            // 
            // btDelete
            // 
            this.btDelete.Location = new System.Drawing.Point(531, 570);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(175, 52);
            this.btDelete.TabIndex = 8;
            this.btDelete.Text = "Delete";
            this.btDelete.UseVisualStyleBackColor = true;
            this.btDelete.Click += new System.EventHandler(this.BtDelete_Click);
            // 
            // btSelect
            // 
            this.btSelect.Location = new System.Drawing.Point(275, 570);
            this.btSelect.Name = "btSelect";
            this.btSelect.Size = new System.Drawing.Size(239, 52);
            this.btSelect.TabIndex = 5;
            this.btSelect.Text = "Select";
            this.btSelect.UseVisualStyleBackColor = true;
            this.btSelect.Click += new System.EventHandler(this.BtSelect_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataGridView2);
            this.groupBox3.Location = new System.Drawing.Point(531, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(369, 545);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tags Select";
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colsTags});
            this.dataGridView2.Location = new System.Drawing.Point(0, 20);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(369, 525);
            this.dataGridView2.TabIndex = 0;
            this.dataGridView2.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.DataGridView2_UserDeletedRow);
            // 
            // colsTags
            // 
            this.colsTags.HeaderText = "Selected Tags";
            this.colsTags.Name = "colsTags";
            this.colsTags.Width = 300;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.treeView1);
            this.groupBox2.Location = new System.Drawing.Point(9, 94);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(260, 528);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Groups View";
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(1, 24);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(259, 504);
            this.treeView1.TabIndex = 0;
            this.treeView1.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.TreeView1_NodeMouseClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btDisConnectCon);
            this.groupBox1.Controls.Add(this.txtIpCon);
            this.groupBox1.Controls.Add(this.BtConnectCon);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtServerNameCon);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(506, 82);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Config Connect";
            // 
            // btDisConnectCon
            // 
            this.btDisConnectCon.Location = new System.Drawing.Point(294, 43);
            this.btDisConnectCon.Name = "btDisConnectCon";
            this.btDisConnectCon.Size = new System.Drawing.Size(170, 20);
            this.btDisConnectCon.TabIndex = 5;
            this.btDisConnectCon.Text = "DisConnect";
            this.btDisConnectCon.UseVisualStyleBackColor = true;
            this.btDisConnectCon.Click += new System.EventHandler(this.btDisConnectCon_Click);
            // 
            // txtIpCon
            // 
            this.txtIpCon.Location = new System.Drawing.Point(87, 22);
            this.txtIpCon.Name = "txtIpCon";
            this.txtIpCon.Size = new System.Drawing.Size(184, 20);
            this.txtIpCon.TabIndex = 2;
            this.txtIpCon.Text = "127.0.0.1";
            // 
            // BtConnectCon
            // 
            this.BtConnectCon.Location = new System.Drawing.Point(294, 22);
            this.BtConnectCon.Name = "BtConnectCon";
            this.BtConnectCon.Size = new System.Drawing.Size(170, 20);
            this.BtConnectCon.TabIndex = 4;
            this.BtConnectCon.Text = "Connect";
            this.BtConnectCon.UseVisualStyleBackColor = true;
            this.BtConnectCon.Click += new System.EventHandler(this.BtConnectCon_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(63, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ip :";
            // 
            // txtServerNameCon
            // 
            this.txtServerNameCon.Location = new System.Drawing.Point(87, 43);
            this.txtServerNameCon.Name = "txtServerNameCon";
            this.txtServerNameCon.Size = new System.Drawing.Size(184, 20);
            this.txtServerNameCon.TabIndex = 3;
            this.txtServerNameCon.Text = "Kepware.KEPServerEX.V6";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "S. Name :";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 666);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.gbSetConnect.ResumeLayout(false);
            this.gbSetConnect.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.Button btSelect;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtIpCon;
        private System.Windows.Forms.Button BtConnectCon;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtServerNameCon;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.GroupBox gbSetConnect;
        private System.Windows.Forms.TextBox txtIp;
        private System.Windows.Forms.Button btConnect;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtServerName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btRunService;
        private System.Windows.Forms.Button btStopService;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtgKepwareTag;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtgValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtgStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtgTime;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colsTags;
        private System.Windows.Forms.Button btDisConnectCon;
    }
}

