﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OPCAutomation;

namespace Kepware_OPC_DA_V1
{
    public partial class Form1 : Form
    {
        public OPCServer MyOPCServer1;
        public OPCGroup MyOPCGroup1;

        public OPCServer MyOPCServer;
        public OPCGroup MyOPCGroup;
        public OPCItem MyOPCItem;
        public OPCBrowser MyOPCBrowser;

        public string HostName = "127.0.0.1";
        public string Kepware = "Kepware.KEPServerEX.V6";
        public string Tag = "Modbus.Device1.A0";

        public Array MyServerHandles = Array.CreateInstance(typeof(int), 1);
        public Array MyValues = Array.CreateInstance(typeof(object), 1);
        public Array MyErrors;

        public List<Kepware_Tag> KepDataRead = new List<Kepware_Tag>();
        public List<Kepware_Tag> KepDataWrite = new List<Kepware_Tag>();
        public DataTable DataRead;

        public string treePath = "";
        public int DataRow = 0;
        public int DeleteIndex;

        public Form1()
        {
            InitializeComponent();
            MyOPCServer1 = new OPCServer();
            MyOPCServer = new OPCServer();
        }

        private void BtConnect_Click(object sender, EventArgs e)
        {
            btStopService.BackColor = default(Color);
            dataGridView3.Rows.Clear();
            KepDataRead.Clear();
            try
            {
                MyOPCServer1.Connect(TxtServerName.Text, txtIp.Text);
                btConnect.BackColor = Color.LimeGreen;

                //var temp = SystemData.ReadTxtfile("KepTag.Json");
                //KepDataRead = SystemData.JsontoObj<List<Kepware_Tag>>(temp);

                DataRead = ConnectDatabase.CallStore("aaa");
                foreach (DataRow dtRow in DataRead.Rows)
                {
                    Kepware_Tag tag = new Kepware_Tag();
                    tag.KepwareTagIndex = int.Parse(dtRow["ID"].ToString().Trim());
                    tag.KepwareTag = dtRow["MachineTag"].ToString().Trim();
                    KepDataRead.Add(tag);
                }

                MyOPCGroup1 = MyOPCServer1.OPCGroups.Add("MyGroup");
                MyOPCGroup1.IsActive = true;
                MyOPCGroup1.IsSubscribed = true;
                MyOPCGroup1.UpdateRate = 1000;

                for (int i = 0; i < KepDataRead.Count; i++)
                {
                    dataGridView3.Rows.Add(new object[] { KepDataRead[i].KepwareTag, "", "", "" });
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }


        private void ObjOPCGroup_DataChange(int TransectionId, int NumItems, ref Array ClientHandles, ref Array ItemValues, ref Array Qualities, ref Array TimeStamps)
        {
            int z = Int32.Parse(ClientHandles.GetValue(1).ToString());
            dataGridView3[1, z].Value = ItemValues.GetValue(1).ToString();
            if (Qualities.GetValue(1).ToString() == "192")
            {
                dataGridView3[2, z].Value = "Good";
            }
            else
            {
                dataGridView3[2, z].Value = "Bad";
            }
            dataGridView3[3, z].Value = TimeStamps.GetValue(1).ToString();
        }

        private void ObjOPCGroup_AsyncReadComplete(int TransectionId, int NumItems, ref Array ClientHandles, ref Array ItemValues, ref Array Qualities, ref Array TimeStamps, ref Array Errors)
        {

        }

        private void ObjOPCGroup_AsyncWriteComplete(int TransectionId, int NumItems, ref Array ClientHandles, ref Array Errors)
        {

        }

        private void BtRunService_Click(object sender, EventArgs e)
        {
            try
            {
                MyOPCGroup1.DataChange += new DIOPCGroupEvent_DataChangeEventHandler(ObjOPCGroup_DataChange);

                for (int i = 0; i < KepDataRead.Count; i++)
                {
                    MyOPCItem = MyOPCGroup1.OPCItems.AddItem(KepDataRead[i].KepwareTag, KepDataRead[i].KepwareTagIndex);
                }
                btRunService.BackColor = Color.LimeGreen;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void BtStopService_Click(object sender, EventArgs e)
        {
            btConnect.BackColor = default(Color);
            btRunService.BackColor = default(Color);
            dataGridView3.Rows.Clear();
            MyOPCGroup1.IsActive = false;
            MyOPCGroup1.IsSubscribed = false;
            MyOPCServer1.OPCGroups.RemoveAll();
            btStopService.BackColor = Color.Red;
        }

        private void BtConnectCon_Click(object sender, EventArgs e)
        {
            try
            {
                MyOPCServer.Connect(txtServerNameCon.Text, txtIpCon.Text);
                MyOPCBrowser = MyOPCServer.CreateBrowser();
                MyOPCBrowser.MoveToRoot();
                Show_Branches(MyOPCBrowser, treeView1.SelectedNode);
                BtConnectCon.BackColor = Color.LimeGreen;
                btDisConnectCon.BackColor = default(Color);
            }
            catch
            {

            }
        }

        private void TreeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            treeView1.SelectedNode = treeView1.GetNodeAt(e.X, e.Y);
            try
            {
                var nPath = e.Node.FullPath.ToString();
                if (nPath == treePath)
                {

                }
                else
                {
                    treePath = nPath;
                }
                string [] sPath = nPath.Split('\\');
                int branchCount = sPath.Length;

                MyOPCBrowser = MyOPCServer.CreateBrowser();
                MyOPCBrowser.MoveToRoot();

                for(int i=0; i< branchCount;i++)
                {
                    MyOPCBrowser.ShowBranches();
                    MyOPCBrowser.MoveDown(sPath[i]);
                }
                Show_Leafs(MyOPCBrowser, treeView1.SelectedNode);
                Show_Branches(MyOPCBrowser, treeView1.SelectedNode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void Show_Leafs(OPCBrowser oPCBrowser, TreeNode aNode)
        {
            aNode.Nodes.Clear();
            listBox1.Items.Clear();
            oPCBrowser.ShowLeafs();

            for (int i=0; i< oPCBrowser.Count;i++)
            {
                var Name = oPCBrowser.Item(i + 1);
                var FullMName = oPCBrowser.GetItemID(Name);
                listBox1.Items.Add(FullMName);
                //aNode.Nodes.Add(oPCBrowser.Item(i + 1).ToString()); ;
            }
        }

        private void Show_Branches(OPCBrowser oPCBrowser, TreeNode aNode)
        {
            oPCBrowser.ShowBranches();
            if (aNode == null)
            {
                treeView1.Nodes.Clear();
            }
            for(int i=0; i< oPCBrowser.Count;i++)
            {
                try
                {
                    string sName = oPCBrowser.Item(i + 1).ToString();
                    if(aNode == null)
                    {
                        treeView1.Nodes.Add(sName);
                    }
                    else
                    {
                        aNode.Nodes.Add(sName);
                    }

                    oPCBrowser.MoveDown(sName);
                    oPCBrowser.ShowBranches();

                    int Ib = oPCBrowser.Count;
                    if (Ib > 0)
                    {
                        if (aNode == null)
                        {
                            treeView1.Nodes[i].Nodes.Add("-");
                        }
                        else
                        {
                            aNode.Nodes[i].Nodes.Add("-");
                        }
                    }
                    oPCBrowser.MoveUp();
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                oPCBrowser.ShowBranches();
            }
            
        }

        private void BtSelect_Click(object sender, EventArgs e)
        {
            int Index = listBox1.SelectedIndex;
            dataGridView2.Rows.Add();
            dataGridView2[0,DataRow].Value = listBox1.Items[Index];
            string test = listBox1.Items[Index].ToString();
            DataRow++;
        }

        private void BtDelete_Click(object sender, EventArgs e)
        {
            DataRow--;
            DeleteIndex = dataGridView2.CurrentCell.RowIndex;
            dataGridView2.Rows.RemoveAt(DeleteIndex);
        }

        private void BtSave_Click(object sender, EventArgs e)
        {
            KepDataWrite.Clear();
            for (int i= 0; i< dataGridView2.Rows.Count - 1; i++)
            {
                Kepware_Tag _Tag = new Kepware_Tag();
                _Tag.KepwareTagIndex = i;
                _Tag.KepwareTag = dataGridView2.Rows[i].Cells["colsTags"].Value.ToString();
                KepDataWrite.Add(_Tag);
            }

            var temp = SystemData.ObjToJson(KepDataWrite);
            SystemData.SaveTxt2file("KepTag.Json", temp);
            MessageBox.Show("Successful Save File");
        }

        private void btDisConnectCon_Click(object sender, EventArgs e)
        {
            dataGridView2.Rows.Clear();
            btDisConnectCon.BackColor = Color.Red;
            BtConnectCon.BackColor = default(Color);
            MyOPCServer.OPCGroups.RemoveAll();
            treeView1.Nodes.Clear();
        }

        private void DataGridView2_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            DataRow--;
        }

        //private void Button5_Click(object sender, EventArgs e)
        //{
        //    List<Kepware_Tag> testtags = new List<Kepware_Tag>();
        //    Kepware_Tag a = new Kepware_Tag();
        //    a.KepwareTag = "Modbus.Device1.A0";
        //    a.KepwareTagIndex = 0;
        //    Kepware_Tag b = new Kepware_Tag();
        //    b.KepwareTag = "Modbus.Device1.A1";
        //    b.KepwareTagIndex = 1;
        //    Kepware_Tag c = new Kepware_Tag();
        //    c.KepwareTag = "Modbus.Device1.A2";
        //    c.KepwareTagIndex = 2;
        //    Kepware_Tag d = new Kepware_Tag();
        //    d.KepwareTag = "Modbus.Device1.A3";
        //    d.KepwareTagIndex = 3;
        //    Kepware_Tag f = new Kepware_Tag();
        //    f.KepwareTag = "Modbus.Device1.A4";
        //    f.KepwareTagIndex = 4;
        //    Kepware_Tag g = new Kepware_Tag();
        //    g.KepwareTag = "Modbus.Device1.A5";
        //    g.KepwareTagIndex = 5;
        //    Kepware_Tag h = new Kepware_Tag();
        //    h.KepwareTag = "Modbus.Device1.A6";
        //    h.KepwareTagIndex = 6;
        //    Kepware_Tag i = new Kepware_Tag();
        //    i.KepwareTag = "Modbus.Device1.A7";
        //    i.KepwareTagIndex = 7;
        //    Kepware_Tag j = new Kepware_Tag();
        //    j.KepwareTag = "Modbus.Device1.A8";
        //    j.KepwareTagIndex = 8;
        //    Kepware_Tag k = new Kepware_Tag();
        //    k.KepwareTag = "Modbus.Device1.A9";
        //    k.KepwareTagIndex = 9;
        //    testtags.Add(a);
        //    testtags.Add(b);
        //    testtags.Add(c);
        //    testtags.Add(d);
        //    testtags.Add(f);
        //    testtags.Add(g);
        //    testtags.Add(h);
        //    testtags.Add(i);
        //    testtags.Add(j);
        //    testtags.Add(k);
        //    var textcombo = SystemData.ObjToJson(testtags);
        //    SystemData.SaveTxt2file("KepTag.Json", textcombo);
        //}
    }
}
