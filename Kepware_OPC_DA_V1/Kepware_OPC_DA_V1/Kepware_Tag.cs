﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kepware_OPC_DA_V1
{
    public class Kepware_Tag
    {
        public string KepwareTag { get; set; }
        public int KepwareTagIndex { get; set; }

    }
}
